package com.practicebd.repository;

import com.practicebd.models.EmployeesModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeesRepository extends JpaRepository<EmployeesModel, Integer> {
}
