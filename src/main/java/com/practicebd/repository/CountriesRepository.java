package com.practicebd.repository;

import com.practicebd.models.CountriesModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountriesRepository extends JpaRepository<CountriesModel, Integer> {
}
