package com.practicebd.repository;

import com.practicebd.models.JobsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobsRepository extends JpaRepository<JobsModel, Integer> {
}
