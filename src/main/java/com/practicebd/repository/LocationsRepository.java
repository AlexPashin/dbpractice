package com.practicebd.repository;

import com.practicebd.models.LocationsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationsRepository extends JpaRepository<LocationsModel, Integer> {
}
