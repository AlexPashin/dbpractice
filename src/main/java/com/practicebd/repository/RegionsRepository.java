package com.practicebd.repository;

import com.practicebd.models.RegionsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionsRepository extends JpaRepository<RegionsModel, Integer> {
}
