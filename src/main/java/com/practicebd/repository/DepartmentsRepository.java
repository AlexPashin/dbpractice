package com.practicebd.repository;

import com.practicebd.models.DepartmentsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentsRepository extends JpaRepository<DepartmentsModel, Integer> {
}
