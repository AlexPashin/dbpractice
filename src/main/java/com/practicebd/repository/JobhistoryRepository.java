package com.practicebd.repository;

import com.practicebd.models.JobhistoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobhistoryRepository extends JpaRepository<JobhistoryModel, Integer> {
}
