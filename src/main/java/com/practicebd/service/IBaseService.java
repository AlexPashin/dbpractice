package com.practicebd.service;

import java.util.List;

public interface IBaseService<T> {
    T add(T element);
    void delete(int id);
    T getByName(String name);
    T editElement(T element);
    List<T> getAll();
}
