package com.practicebd.service.impl;

import com.practicebd.models.DepartmentsModel;
import com.practicebd.repository.DepartmentsRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentsService implements IBaseService<DepartmentsModel> {

    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Override
    public DepartmentsModel add(DepartmentsModel element) { return departmentsRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        departmentsRepository.deleteById(id);
    }

    @Override
    public DepartmentsModel getByName(String name) {
        return null;
    }

    @Override
    public DepartmentsModel editElement(DepartmentsModel element) { return departmentsRepository.saveAndFlush(element); }

    @Override
    public List<DepartmentsModel> getAll() {
        return departmentsRepository.findAll();
    }
}
