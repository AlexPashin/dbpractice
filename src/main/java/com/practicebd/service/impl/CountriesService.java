package com.practicebd.service.impl;

import com.practicebd.models.CountriesModel;
import com.practicebd.repository.CountriesRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountriesService implements IBaseService<CountriesModel> {

    @Autowired
    private CountriesRepository countriesRepository;

    @Override
    public CountriesModel add(CountriesModel element) { return countriesRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        countriesRepository.deleteById(id);
    }

    @Override
    public CountriesModel getByName(String name) {
        return null;
    }

    @Override
    public CountriesModel editElement(CountriesModel element) {
        return countriesRepository.saveAndFlush(element);
    }

    @Override
    public List<CountriesModel> getAll() {
        return countriesRepository.findAll();
    }
}
