package com.practicebd.service.impl;

import com.practicebd.models.LocationsModel;
import com.practicebd.repository.LocationsRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationsService implements IBaseService<LocationsModel> {

    @Autowired
    private LocationsRepository locationsRepository;

    @Override
    public LocationsModel add(LocationsModel element) { return locationsRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        locationsRepository.deleteById(id);
    }

    @Override
    public LocationsModel getByName(String name) {
        return null;
    }

    @Override
    public LocationsModel editElement(LocationsModel element) { return locationsRepository.saveAndFlush(element); }

    @Override
    public List<LocationsModel> getAll() {
        return locationsRepository.findAll();
    }
}
