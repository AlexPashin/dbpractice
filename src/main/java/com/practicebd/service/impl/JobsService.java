package com.practicebd.service.impl;

import com.practicebd.models.JobsModel;
import com.practicebd.repository.JobsRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobsService implements IBaseService<JobsModel> {

    @Autowired
    private JobsRepository jobsRepository;

    @Override
    public JobsModel add(JobsModel element) { return jobsRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        jobsRepository.deleteById(id);
    }

    @Override
    public JobsModel getByName(String name) {
        return null;
    }

    @Override
    public JobsModel editElement(JobsModel element) { return jobsRepository.saveAndFlush(element); }

    @Override
    public List<JobsModel> getAll() {
        return jobsRepository.findAll();
    }
}
