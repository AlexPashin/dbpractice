package com.practicebd.service.impl;

import com.practicebd.models.JobhistoryModel;
import com.practicebd.repository.JobhistoryRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobhistoryService implements IBaseService<JobhistoryModel> {

    @Autowired
    private JobhistoryRepository jobhistoryRepository;

    @Override
    public JobhistoryModel add(JobhistoryModel element) { return jobhistoryRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        jobhistoryRepository.deleteById(id);
    }

    @Override
    public JobhistoryModel getByName(String name) {
        return null;
    }

    @Override
    public JobhistoryModel editElement(JobhistoryModel element) { return jobhistoryRepository.saveAndFlush(element); }

    @Override
    public List<JobhistoryModel> getAll() {
        return jobhistoryRepository.findAll();
    }
}
