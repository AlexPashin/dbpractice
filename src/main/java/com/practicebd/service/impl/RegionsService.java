package com.practicebd.service.impl;

import com.practicebd.models.RegionsModel;
import com.practicebd.repository.RegionsRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionsService implements IBaseService<RegionsModel> {

    @Autowired
    private RegionsRepository regionsRepository;

    @Override
    public RegionsModel add(RegionsModel element) { return regionsRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        regionsRepository.deleteById(id);
    }

    @Override
    public RegionsModel getByName(String name) {
        return null;
    }

    @Override
    public RegionsModel editElement(RegionsModel element) { return regionsRepository.saveAndFlush(element); }

    @Override
    public List<RegionsModel> getAll() {
        return regionsRepository.findAll();
    }
}
