package com.practicebd.service.impl;

import com.practicebd.models.EmployeesModel;
import com.practicebd.repository.EmployeesRepository;
import com.practicebd.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeesService implements IBaseService<EmployeesModel> {

    @Autowired
    @Qualifier("employeesRepository")
    private EmployeesRepository employeesRepository;

    @Override
    public EmployeesModel add(EmployeesModel element) { return employeesRepository.saveAndFlush(element); }

    @Override
    public void delete(int id) {
        employeesRepository.deleteById(id);
    }

    @Override
    public EmployeesModel getByName(String name) {
        return null;
    }

    @Override
    public EmployeesModel editElement(EmployeesModel element) { return employeesRepository.saveAndFlush(element); }

    @Override
    public List<EmployeesModel> getAll() {
        return employeesRepository.findAll();
    }
}
