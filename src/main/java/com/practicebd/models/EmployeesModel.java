package com.practicebd.models;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "employees")
public class EmployeesModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "employee_id", nullable = false)
    private Integer employee_id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name", nullable = false)
    private String last_name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "phone_number")
    private String phone_number;

    @Column(name = "hire_date", nullable = false)
    private String hire_date;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "job_id", nullable = false)
    private JobsModel job_id;

    @Column(name = "salary")
    private Double salary;

    @Column(name = "commission_pct")
    private Double commission_pct;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "manager_id")
    private EmployeesModel manager_id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "department_id")
    private DepartmentsModel department_id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "manager_id")
    private Set<EmployeesModel> employeesModelSet;

    @OneToOne(mappedBy = "manager_id")
    public DepartmentsModel departmentsModel;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "employee_id")
    private Set<JobhistoryModel> jobhistoryModelSet;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public EmployeesModel(){
    }

    public EmployeesModel(Integer employee_id, String first_name, String last_name, String email, String phone_number, String hire_date, JobsModel job_id, Double salary, Double commission_pct, EmployeesModel manager_id, DepartmentsModel department_id, /*EmployeesModel...employees,*/ DepartmentsModel departmentsModel, JobhistoryModel...jobhistorys) {
        this.employee_id = employee_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
        this.hire_date = hire_date;
        this.job_id = job_id;
        this.salary = salary;
        this.commission_pct = commission_pct;
        this.manager_id = manager_id;
        this.department_id = department_id;
        /*this.employeesModelSet = Stream.of(employees).collect(Collectors.toSet());
        this.employeesModelSet.forEach(x -> x.setManager_id(this));*/
        this.departmentsModel = departmentsModel;
        this.jobhistoryModelSet = Stream.of(jobhistorys).collect(Collectors.toSet());
        this.jobhistoryModelSet.forEach(x -> x.setEmployee_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getHire_date() {
        return hire_date;
    }

    public void setHire_date(String hire_date) {
        this.hire_date = hire_date;
    }

    public JobsModel getJob_id() {
        return job_id;
    }

    public void setJob_id(JobsModel job_id) {
        this.job_id = job_id;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getCommission_pct() {
        return commission_pct;
    }

    public void setCommission_pct(Double commission_pct) {
        this.commission_pct = commission_pct;
    }

    public EmployeesModel getManager_id() {
        return manager_id;
    }

    public void setManager_id(EmployeesModel manager_id) {
        this.manager_id = manager_id;
    }

    public DepartmentsModel getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(DepartmentsModel department_id) {
        this.department_id = department_id;
    }

    public Set<EmployeesModel> getEmployeesModelSet() {
        return employeesModelSet;
    }

    public void setEmployeesModelSet(Set<EmployeesModel> employeesModelSet) {
        this.employeesModelSet = employeesModelSet;
    }

    public DepartmentsModel getDepartmentsModel() {
        return departmentsModel;
    }

    public void setDepartmentsModel(DepartmentsModel departmentsModel) {
        this.departmentsModel = departmentsModel;
    }

    public Set<JobhistoryModel> getJobhistoryModelSet() {
        return jobhistoryModelSet;
    }

    public void setJobhistoryModelSet(Set<JobhistoryModel> jobhistoryModelSet) {
        this.jobhistoryModelSet = jobhistoryModelSet;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "EmployeesModel{" +
                "employee_id=" + employee_id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", hire_date='" + hire_date + '\'' +
                ", job_id=" + job_id +
                ", salary=" + salary +
                ", commission_pct=" + commission_pct +
                ", manager_id=" + manager_id +
                ", department_id=" + department_id +
                ", employeesModelSet=" + employeesModelSet +
                ", departmentsModel=" + departmentsModel +
                ", jobhistoryModelSet=" + jobhistoryModelSet +
                '}';
    }
}
