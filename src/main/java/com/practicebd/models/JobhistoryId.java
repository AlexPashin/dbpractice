package com.practicebd.models;

import java.io.Serializable;
import java.util.Objects;

public class JobhistoryId implements Serializable {

    private Integer employee_id;
    private String start_date;

    //<editor-fold desc="Constructors">

    public JobhistoryId() {

    }

    public JobhistoryId(Integer employee_id, String start_date) {
        this.employee_id = employee_id;
        this.start_date = start_date;
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }


    //</editor-fold>


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobhistoryId)) return false;
        JobhistoryId that = (JobhistoryId) o;
        return employee_id.equals(that.employee_id) &&
                start_date.equals(that.start_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employee_id, start_date);
    }
}