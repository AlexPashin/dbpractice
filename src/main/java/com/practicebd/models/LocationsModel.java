package com.practicebd.models;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "locations")
public class LocationsModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "location_id", nullable = false)
    private Integer location_id;

    @Column(name = "street_address")
    private String street_address;

    @Column(name = "postal_code")
    private String postal_code;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state_province")
    private String state_province;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "country_id", nullable = false)
    private CountriesModel country_id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "location_id")
    private List<DepartmentsModel> departmentsModelSet;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public LocationsModel() {
    }

    public LocationsModel(Integer location_id, String street_address, String postal_code, String city, String state_province, CountriesModel country_id, List<DepartmentsModel> departmentsModels) {
        this.location_id = location_id;
        this.street_address = street_address;
        this.postal_code = postal_code;
        this.city = city;
        this.state_province = state_province;
        this.country_id = country_id;
        this.departmentsModelSet = departmentsModels;
        this.departmentsModelSet.forEach(x -> x.setLocation_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public Integer getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public CountriesModel getCountry_id() {
        return country_id;
    }

    public void setCountry_id(CountriesModel country_id) {
        this.country_id = country_id;
    }

    public Set<DepartmentsModel> getDepartmentsModelSet() {
        return departmentsModelSet;
    }

    public void setDepartmentsModelSet(Set<DepartmentsModel> departmentsModelSet) {
        this.departmentsModelSet = departmentsModelSet;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "LocationsModel{" +
                "location_id=" + location_id +
                ", street_address='" + street_address + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", city='" + city + '\'' +
                ", state_province='" + state_province + '\'' +
                ", country_id=" + country_id +
                ", departmentsModelSet=" + departmentsModelSet +
                '}';
    }
}