package com.practicebd.models;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "departments")
public class DepartmentsModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "department_id", nullable = false)
    private Integer department_id;

    @Column(name = "department_name", nullable = false)
    private String department_name;

    @OneToOne(optional = false)
    @JoinColumn(name = "manager_id", unique = true, updatable = false)
    private EmployeesModel manager_id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "location_id", nullable = false)
    private LocationsModel location_id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "department_id")
    private Set<EmployeesModel> employeesModelSet;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "department_id")
    private Set<JobhistoryModel> jobhistoryModels;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public DepartmentsModel(){
    }

    public DepartmentsModel(Integer department_id, String department_name, EmployeesModel manager_id, LocationsModel location_id, /*EmployeesModel...employees,*/ JobhistoryModel...jobhistorys) {
        this.department_id = department_id;
        this.department_name = department_name;
        this.manager_id = manager_id;
        this.location_id = location_id;
        /*this.employeesModelSet = Stream.of(employees).collect(Collectors.toSet());
        this.employeesModelSet.forEach(x -> x.setDepartment_id(this));*/
        this.jobhistoryModels = Stream.of(jobhistorys).collect(Collectors.toSet());
        this.jobhistoryModels.forEach(x -> x.setDepartment_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public Integer getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public EmployeesModel getManager_id() {
        return manager_id;
    }

    public void setManager_id(EmployeesModel manager_id) {
        this.manager_id = manager_id;
    }

    public LocationsModel getLocation_id() {
        return location_id;
    }

    public void setLocation_id(LocationsModel location_id) {
        this.location_id = location_id;
    }

    public Set<EmployeesModel> getEmployeesModelSet() {
        return employeesModelSet;
    }

    public void setEmployeesModelSet(Set<EmployeesModel> employeesModelSet) {
        this.employeesModelSet = employeesModelSet;
    }

    public Set<JobhistoryModel> getJobhistoryModels() {
        return jobhistoryModels;
    }

    public void setJobhistoryModels(Set<JobhistoryModel> jobhistoryModels) {
        this.jobhistoryModels = jobhistoryModels;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "DepartmentsModel{" +
                "department_id=" + department_id +
                ", department_name='" + department_name + '\'' +
                ", manager_id=" + manager_id +
                ", location_id=" + location_id +
                ", employeesModelSet=" + employeesModelSet +
                ", jobhistoryModels=" + jobhistoryModels +
                '}';
    }
}
