package com.practicebd.models;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "countries")
public class CountriesModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "country_id", nullable = false)
    private String country_id;

    @Column(name = "country_name")
    private String country_name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "region_id", nullable = false)
    private RegionsModel region_id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "country_id")
    private Set<LocationsModel> locationModelSet;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public CountriesModel(){
    }

    public CountriesModel(String country_id, String country_name, RegionsModel region_id, LocationsModel...locations) {
        this.country_id = country_id;
        this.country_name = country_name;
        this.region_id = region_id;
        this.locationModelSet = Stream.of(locations).collect(Collectors.toSet());
        this.locationModelSet.forEach(x -> x.setCountry_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public RegionsModel getRegion_id() {
        return region_id;
    }

    public void setRegion_id(RegionsModel region_id) {
        this.region_id = region_id;
    }

    public Set<LocationsModel> getLocationModelSet() {
        return locationModelSet;
    }

    public void setLocationModelSet(Set<LocationsModel> locationModelSet) {
        this.locationModelSet = locationModelSet;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "CountriesModel{" +
                "country_id='" + country_id + '\'' +
                ", country_name='" + country_name + '\'' +
                ", region_id=" + region_id +
                ", locationModelSet=" + locationModelSet +
                '}';
    }
}
