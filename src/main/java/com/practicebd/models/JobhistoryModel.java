package com.practicebd.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "job_history")
@IdClass(JobhistoryId.class)
public class JobhistoryModel {

    //<editor-fold desc="Fields">

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @Id
    @JoinColumn(name = "employee_id", nullable = false)
    private EmployeesModel employee_id;

    @Id
    @Column(name = "start_date", nullable = false)
    private String start_date;

    @Column(name = "end_date", nullable = false)
    private String end_date;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "job_id", nullable = false)
    private JobsModel job_id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "department_id", nullable = false)
    private DepartmentsModel department_id;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public JobhistoryModel() {
    }

    public JobhistoryModel(EmployeesModel employee_id, String start_date, String end_date, JobsModel job_id, DepartmentsModel department_id) {
        this.employee_id = employee_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.job_id = job_id;
        this.department_id = department_id;
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public EmployeesModel getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(EmployeesModel employee_id) {
        this.employee_id = employee_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public JobsModel getJob_id() {
        return job_id;
    }

    public void setJob_id(JobsModel job_id) {
        this.job_id = job_id;
    }

    public DepartmentsModel getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(DepartmentsModel department_id) {
        this.department_id = department_id;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "JobhistoryModel{" +
                "employee_id=" + employee_id +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", job_id=" + job_id +
                ", department_id=" + department_id +
                '}';
    }
}