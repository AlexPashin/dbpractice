package com.practicebd.models;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "jobs")
public class JobsModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "job_id", nullable = false)
    private String job_id;

    @Column(name = "job_title", nullable = false)
    private String job_title;

    @Column(name = "min_salary")
    private Integer min_salary;

    @Column(name = "max_salary")
    private Integer max_salary;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "job_id")
    private Set<EmployeesModel> employeesModelSet;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "job_id")
    private Set<JobhistoryModel> jobhistoryModelSet;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public JobsModel() {
    }

    public JobsModel(String job_id, String job_title, Integer min_salary, Integer max_salary/*, List<EmployeesModel> employees*/, JobhistoryModel...jobhistorys) {
        this.job_id = job_id;
        this.job_title = job_title;
        this.min_salary = min_salary;
        this.max_salary = max_salary;
        /*this.employeesModelSet = Stream.of(employees).collect(Collectors.toSet());
        this.employeesModelSet.forEach(x -> x.setJob_id(this));*/
        this.jobhistoryModelSet = Stream.of(jobhistorys).collect(Collectors.toSet());
        this.jobhistoryModelSet.forEach(x -> x.setJob_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public Integer getMin_salary() {
        return min_salary;
    }

    public void setMin_salary(Integer min_salary) {
        this.min_salary = min_salary;
    }

    public Integer getMax_salary() {
        return max_salary;
    }

    public void setMax_salary(Integer max_salary) {
        this.max_salary = max_salary;
    }

    public Set<EmployeesModel> getEmployeesModelSet() {
        return employeesModelSet;
    }

    public void setEmployeesModelSet(Set<EmployeesModel> employeesModelSet) {
        this.employeesModelSet = employeesModelSet;
    }

    public Set<JobhistoryModel> getJobhistoryModelSet() {
        return jobhistoryModelSet;
    }

    public void setJobhistoryModelSet(Set<JobhistoryModel> jobhistoryModelSet) {
        this.jobhistoryModelSet = jobhistoryModelSet;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "JobsModel{" +
                "job_id='" + job_id + '\'' +
                ", job_title='" + job_title + '\'' +
                ", min_salary=" + min_salary +
                ", max_salary=" + max_salary +
                ", employeesModelSet=" + employeesModelSet +
                ", jobhistoryModelSet=" + jobhistoryModelSet +
                '}';
    }
}