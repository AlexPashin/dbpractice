package com.practicebd.models;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "regions")
public class RegionsModel {

    //<editor-fold desc="Fields">

    @Id
    @Column(name = "region_id", nullable = false)
    private Integer region_id;

    @Column(name = "region_name")
    private String region_name;

    @JoinColumn(referencedColumnName = "region_id")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "region_id", cascade = CascadeType.ALL)
    private Set<CountriesModel> countryModelSet;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public RegionsModel() {
    }

    public RegionsModel(Integer region_id, String region_name, CountriesModel...countries) {
        this.region_id = region_id;
        this.region_name = region_name;
        this.countryModelSet = Stream.of(countries).collect(Collectors.toSet());
        this.countryModelSet.forEach(x -> x.setRegion_id(this));
    }

    //</editor-fold>

    //<editor-fold desc="Getters and Setters">

    public Integer getRegion_id() {
        return region_id;
    }

    public void setRegion_id(Integer region_id) {
        this.region_id = region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public Set<CountriesModel> getCountryModelSet() {
        return countryModelSet;
    }

    public void setCountryModelSet(Set<CountriesModel> countryModelSet) {
        this.countryModelSet = countryModelSet;
    }


    //</editor-fold>

    @Override
    public String toString() {
        return "RegionsModel{" +
                "region_id=" + region_id +
                ", region_name='" + region_name + '\'' +
                ", countryModelSet=" + countryModelSet +
                '}';
    }
}