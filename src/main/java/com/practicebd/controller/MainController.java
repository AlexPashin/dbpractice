package com.practicebd.controller;

import com.practicebd.models.*;
import com.practicebd.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    EmployeesService employeesService;

    @Autowired
    DepartmentsService departmentsService;

    @Autowired
    LocationsService locationsService;

    @Autowired
    CountriesService countriesService;

    @Autowired
    RegionsService regionsService;

    @Autowired
    JobsService jobsService;

    @Autowired
    JobhistoryService jobhistoryService;

    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping(value = "/employeesList")
    public String employeesList(
            Model model){
        List<EmployeesModel> employees = employeesService.getAll();
        model.addAttribute("employeesList", employees);
        return "employeesTable";
    }

    @GetMapping(value = "/departmentsList")
    public String departmentsList(
            Model model){
        List<DepartmentsModel> departments = departmentsService.getAll();
        model.addAttribute("departmentsList", departments);
        return "departmentsTable";
    }

    @GetMapping(value = "/locationsList")
    public String locationsList(
            Model model){
        List<LocationsModel> locations = locationsService.getAll();
        model.addAttribute("locationsList", locations);
        return "locationsTable";
    }

    @GetMapping(value = "/countriesList")
    public String countriesList(
            Model model){
        List<CountriesModel> countries = countriesService.getAll();
        model.addAttribute("countriesList", countries);
        return "countriesTable";
    }

    @GetMapping(value = "/regionsList")
    public String regionsList(
            Model model){
        List<RegionsModel> regions = regionsService.getAll();
        model.addAttribute("regionsList", regions);
        return "regionsTable";
    }

    @GetMapping(value = "/jobsList")
    public String jobsList(
            Model model){
        List<JobsModel> jobs = jobsService.getAll();
        model.addAttribute("jobsList", jobs);
        return "jobsTable";
    }

    @GetMapping(value = "/jobhistoryList")
    public String jobhistoryList(
            Model model){
        List<JobhistoryModel> jobhistory = jobhistoryService.getAll();
        model.addAttribute("jobhistoryList", jobhistory);
        return "jobhistoryTable";
    }
    @GetMapping("/addRegion")
    public String addMan(Model model) {

        List<RegionsModel> regions = regionsService.getAll();
        model.addAttribute("regionsList", regions);

        return "/addRegion";
    }
}