package com.practicebd.controller;

import com.practicebd.models.*;
import com.practicebd.service.IBaseService;
import com.practicebd.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class RestController {

    @Autowired
    EmployeesService employeesService;

    @Autowired
    DepartmentsService departmentsService;

    @Autowired
    LocationsService locationsService;

    @Autowired
    CountriesService countriesService;

    @Autowired
    IBaseService<RegionsModel> regionsService;

    @Autowired
    JobsService jobsService;

    @Autowired
    JobhistoryService jobhistoryService;

    @PostMapping("/rest/addEmployee")
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeesModel addEmployee(@RequestBody EmployeesModel addable) {
        return employeesService.add(addable);
    }
    @PostMapping("/rest/delEmployee")
    @ResponseStatus(HttpStatus.CREATED)
    public void delEmployee(@RequestBody EmployeesModel deletable) {
        employeesService.delete(deletable.getEmployee_id());
    }
    @PostMapping("/rest/chgEmployee")
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeesModel chgEmployee(@RequestBody EmployeesModel changable) {
        return employeesService.editElement(changable);
    }

    @PostMapping("/rest/addDepartment")
    @ResponseStatus(HttpStatus.CREATED)
    public DepartmentsModel addDepartment(@RequestBody DepartmentsModel addable) {
        return departmentsService.add(addable);
    }
    @PostMapping("/rest/delDepartment")
    @ResponseStatus(HttpStatus.CREATED)
    public void delDepartment(@RequestBody DepartmentsModel deletable) {
        departmentsService.delete(deletable.getDepartment_id());
    }
    @PostMapping("/rest/chgDepartment")
    @ResponseStatus(HttpStatus.CREATED)
    public DepartmentsModel chgDepartment(@RequestBody DepartmentsModel changable) {
        return departmentsService.editElement(changable);
    }

    @PostMapping("/rest/addLocation")
    @ResponseStatus(HttpStatus.CREATED)
    public LocationsModel addLocation(@RequestBody LocationsModel addable) {
        return locationsService.add(addable);
    }
    @PostMapping("/rest/delLocation")
    @ResponseStatus(HttpStatus.CREATED)
    public void delLocation(@RequestBody LocationsModel deletable) {
        locationsService.delete(deletable.getLocation_id());
    }
    @PostMapping("/rest/chgLocation")
    @ResponseStatus(HttpStatus.CREATED)
    public LocationsModel chgLocation(@RequestBody LocationsModel changable) {
        return locationsService.editElement(changable);
    }

    @PostMapping("/rest/addCountry")
    @ResponseStatus(HttpStatus.CREATED)
    public CountriesModel addCountry(@RequestBody CountriesModel addable) {
        return countriesService.add(addable);
    }
    /*@PostMapping("/rest/delCountry")
    @ResponseStatus(HttpStatus.CREATED)
    public void delCountry(@RequestBody CountriesModel deletable) {
        countriesService.delete(deletable.getCountry_id());
    }*/
    @PostMapping("/rest/chgCountry")
    @ResponseStatus(HttpStatus.CREATED)
    public CountriesModel chgLocation(@RequestBody CountriesModel changable) {
        return countriesService.editElement(changable);
    }

    @PostMapping("/rest/addRegion")
    @ResponseStatus(HttpStatus.CREATED)
    public RegionsModel addRegion(@RequestBody RegionsModel addable) {
        return regionsService.add(addable);
    }
    @PostMapping("/rest/delRegion")
    @ResponseStatus(HttpStatus.CREATED)
    public void delRegion(@RequestBody RegionsModel deletable) {
        regionsService.delete(deletable.getRegion_id());
    }
    @PostMapping("/rest/chgRegion")
    @ResponseStatus(HttpStatus.CREATED)
    public RegionsModel chgRegion(@RequestBody RegionsModel changable) {
        return regionsService.editElement(changable);
    }

    @PostMapping("/rest/addJob")
    @ResponseStatus(HttpStatus.CREATED)
    public JobsModel addJob(@RequestBody JobsModel addable) {
        return jobsService.add(addable);
    }
    /*@PostMapping("/rest/delJob")
    @ResponseStatus(HttpStatus.CREATED)
    public void delJob(@RequestBody JobsModel deletable) {
        jobsService.delete(deletable.getJob_id());
    }*/
    @PostMapping("/rest/chgJob")
    @ResponseStatus(HttpStatus.CREATED)
    public JobsModel chgJob(@RequestBody JobsModel changable) {
        return jobsService.editElement(changable);
    }

    @PostMapping("/rest/addJobhistory")
    @ResponseStatus(HttpStatus.CREATED)
    public JobhistoryModel addJobhistory(@RequestBody JobhistoryModel addable) {
        return jobhistoryService.add(addable);
    }
    /*@PostMapping("/rest/delJobhistory")
    @ResponseStatus(HttpStatus.CREATED)
    public void delJobhistory(@RequestBody JobhistoryModel deletable) {
        jobhistoryService.delete(deletable.getEmployee_id());
    }*/
    @PostMapping("/rest/chgJobhistory")
    @ResponseStatus(HttpStatus.CREATED)
    public JobhistoryModel chgJobhistory(@RequestBody JobhistoryModel changable) {
        return jobhistoryService.editElement(changable);
    }
}
