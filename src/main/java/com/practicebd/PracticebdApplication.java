package com.practicebd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.practicebd.controller", "com.practicebd.service" })
@SpringBootApplication
public class PracticebdApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticebdApplication.class, args);
	}

}
